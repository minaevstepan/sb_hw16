﻿

#include <iostream>
#include <string>
using namespace std;

class Animal {

public:
     
   virtual void  Voice() {
        cout << "Animal make a sound: "<< "\n";
    };
};

class Cow : public Animal {

public:
    void  Voice() override {
        cout << "Moo " << "\n";
    };
};

class Cat : public Animal {
public:
    void  Voice() override {
        cout << "Myau " << "\n";
    };
};

class Dog : public Animal {
public:
    void  Voice() override {
        cout << "Woof " << "\n";
    };
};

class Rooster : public Animal {
public:
    void  Voice() override {
        cout << "Ku-ka-re-ku " << "\n";
    };
};


int main()
{
    Animal genericAnimal;
    genericAnimal.Voice();

    Cow gCow;
    gCow.Voice();
    Cat gCat;
    gCat.Voice();
    Dog gDog;
    gDog.Voice();
    Rooster gRooster;
    gRooster.Voice();

    cout << " " << "\n";

    Cow* pCow = new Cow;
    Cat* pCat = new Cat;
    Dog* pDog = new Dog;
    Rooster* pRooster = new Rooster;

   const int n = 4;

   Animal* Mas[n] = {pCow,pCat,pDog,pRooster};


    for (int i = 0; i < n; i++) {

        //cout << Mas[i] << "\n";
        Mas[i]->Voice();
    }

    for (int i = 0; i < n; i++) delete Mas[i];

}



//Создать класс Animal с публичным методом Voice() который выводит в консоль строку с текстом.
//Наследовать от Animal минимум три класса(к примеру Dog, Cat и т.д.) и в них перегрузить метод Voice() таким образом, чтобы для примера в классе Dog метод Voice() выводил в консоль "Woof!".
//В функции main создать массив указателей типа Animal и заполнить этот массив объектами созданных классов.
//Затем пройтись циклом по массиву, вызывая на каждом элементе массива метод Voice().
//Протестировать его работу, должны выводиться сообщения из ваших классов наследников Animal.



  ///* Mas[0] = new Cow();
   // Mas[1] = new Cat();
   // Mas[2] = new Dog();
   // Mas[3] = new Rooster();*/